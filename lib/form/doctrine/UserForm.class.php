<?php

/**
 * User form.
 *
 * @package    form
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
class UserForm extends BaseUserForm
{
    public function configure()
    {
        sfContext::getInstance()->getConfiguration()->loadHelpers('I18N');

        $this->setWidget('gender', new sfWidgetFormChoice(
            array(
                'expanded' => true,
                'multiple' => false,
                'choices' => array(
                    'male' => __('male', null, 'form_widgets'),
                    'female' => __('female', null, 'form_widgets')
                )
            )
        ));

        $this->widgetSchema->getFormFormatter()->setTranslationCatalogue('form_widgets');

    }
}
