<?php

/**
 * user actions.
 *
 * @package    form
 * @subpackage user
 * @author     Your name here
 * @version    SVN: $Id$
 */
class userActions extends sfActions
{
    public function preExecute()
    {
        parent::preExecute();

        $request = sfContext::getInstance()->getRequest();

        $this->languages = $request->getLanguages();
        $this->language = $request->getPreferredCulture(array('en', 'ru', 'fr'));
        $this->culture = $this->getUser()->getCulture();
    }
  /**
   * Executes index action
   *
   * @param sfRequest $request A request object
   */
    public function executeIndex(sfWebRequest $request)
    {
    }

    public function executeRegister(sfWebRequest $request)
    {
        $this->form = new UserForm();
    }



}
